This is a minimal template for Excercise sheets originally created for the Cognitive Systems lecture.

It has the most important packages for math and formatting.
The headers with the personal information are created in uebungsblatt_cfg.tex.
There's likely more sophisticated versions of this out there or you might want to create one yourself.

Be encouraged to share and add alternatives or improvements!